__author__ = 'oradba'
import sys
import json
import cx_Oracle
import subprocess


tables = {
    'contact': 'staging_contact',
    'F': 'staging_f_transportation',
    'G': 'staging_g_mechanical',
    'H': 'staging_h_engineering'
}

connect_string = "ers/bullet11!@c3rserstest_beluga"

contact_sql = """
    select * from (
        select *
        from STAGING_CONTACT
        order by DT_SUBMIT desc
    )
    where trunc(dt_submit) = date'{0}'
"""
staging_any_sql = "select * from {0} where log_id = :log_id"


if __name__ == "__main__":

    submit_date = sys.argv[1]
    # open 4 files for writing json data
    c = open('staging_contact_{0}.json'.format(submit_date), 'w')
    f = open('staging_f_transportation_{0}.json'.format(submit_date), 'w')
    g = open('staging_g_mechanical_{0}.json'.format(submit_date), 'w')
    h = open('staging_h_engineering_{0}.json'.format(submit_date), 'w')

    # open a connection to the database and get the staging contact data
    conn = cx_Oracle.connect(connect_string)
    contact_cursor = conn.cursor()
    contact_cursor.execute(contact_sql.format(submit_date))

    for record in contact_cursor:
        contact_dict = dict(zip([x[0].lower() for x in contact_cursor.description], record))
        # convert datetime.datetime to an iso8601 time string
        if contact_dict['dt_submit']:
            contact_dict['dt_submit'] = contact_dict['dt_submit'].isoformat()
        if contact_dict['dt_receive']:
            contact_dict['dt_receive'] = contact_dict['dt_receive'].isoformat()

        # find out which table to get the report_data from. then get it
        table_code = contact_dict.get('log_id').split(' ')[0].split('_')[1]
        table_cursor = conn.cursor()
        table_cursor.execute(staging_any_sql.format(tables.get(table_code)), {'log_id': contact_dict.get('log_id')})
        table_row = table_cursor.fetchone()

        # save the row as a dict
        table_dict = dict(zip([y[0].lower() for y in table_cursor.description], table_row))
        table_cursor.close()

        # Fix/convert Oracle CLOB to string and go from UTF-8 to LATIN-1 encoding to avoid errors in dumping to json
        table_dict['describeevent_narrative'] = str(table_dict['describeevent_narrative']).decode('latin-1')
        table_json = json.dumps(table_dict)

        # figure out which file to write the data to.
        if table_code == 'F':
            f.write(table_json + "\n")
        elif table_code == 'G':
            g.write(table_json + "\n")
        elif table_code == 'H':
            h.write(table_json + "\n")

        # contact data row
        c.write(json.dumps(contact_dict) + "\n")

    contact_cursor.close()
    conn.close()

    c.close()
    f.close()
    g.close()
    h.close()

    # move the files to github
    subprocess.call(['git.exe', 'commit', '-m', 'New test data for {0}'.format(submit_date)])
    subprocess.call(['git.exe', 'push', 'origin', 'master'])


